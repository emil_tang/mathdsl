package org.xtext.example.mydsl.generator

import javax.swing.JOptionPane
import org.eclipse.emf.ecore.resource.Resource
import org.eclipse.xtext.generator.AbstractGenerator
import org.eclipse.xtext.generator.IFileSystemAccess2
import org.eclipse.xtext.generator.IGeneratorContext
import org.xtext.example.mydsl.myDsl.Div
import org.xtext.example.mydsl.myDsl.Exp
import org.xtext.example.mydsl.myDsl.MathExp
import org.xtext.example.mydsl.myDsl.Minus
import org.xtext.example.mydsl.myDsl.Mult
import org.xtext.example.mydsl.myDsl.Number
import org.xtext.example.mydsl.myDsl.Parenthesis
import org.xtext.example.mydsl.myDsl.Plus
import org.xtext.example.mydsl.myDsl.LetExp
import org.xtext.example.mydsl.myDsl.Var
import java.util.Map

class MyDslGenerator extends AbstractGenerator {

	static val Map<String, Integer> symbolTable = newHashMap()

	override void doGenerate(Resource resource, IFileSystemAccess2 fsa, IGeneratorContext context) {
		val math = resource.allContents.filter(MathExp).next
		val result = math.compute
		System::out.println("Math expression = " + math.display)
		// For +1 score, replace with hovering, see Bettini Chapter 8
		JOptionPane::showMessageDialog(null, "result = " + result, "Math Language", JOptionPane::INFORMATION_MESSAGE)
	}

	// Compute function: computes value of expression //
	def dispatch static int compute(LetExp it) {
		val r = right !== null ? right.compute
		symbolTable.put(assignent.symbol, assignent.exp.compute)
		val l = exp.compute
		return switch (operator) {
			Plus: l + r
			Minus: l - r
			Mult: l * r
			Div: l / r
			default: l
		}
	}

	def dispatch static int compute(Var it) { symbolTable.get(variable) }

	def dispatch static int compute(MathExp it) { exp.compute }

	def dispatch static int compute(Exp it) {
		switch operator {
			Plus: left.compute + right.compute
			Minus: left.compute - right.compute
			Mult: left.compute * right.compute
			Div: left.compute / right.compute
			default: left.compute
		}
	}

	def dispatch static int compute(Number it) { value }

	def dispatch static int compute(Parenthesis it) { exp.compute }

	// Display function: show complete syntax tree //
	def dispatch String display(MathExp it) '''Math[«exp.display»]'''

	def dispatch String display(Exp it) '''Exp[«left.display»,«operator?.display»,«right?.display»]'''

	def dispatch String display(Div it) '''/'''

	def dispatch String display(Mult it) '''*'''

	def dispatch String display(Plus it) '''+'''

	def dispatch String display(Minus it) '''-'''

	def dispatch String display(Number it) '''Number[«value»]'''

	def dispatch String display(Parenthesis it) '''(«exp.display»)'''

}
